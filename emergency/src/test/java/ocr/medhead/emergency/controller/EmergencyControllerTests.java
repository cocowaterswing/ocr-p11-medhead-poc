package ocr.medhead.emergency.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import ocr.medhead.emergency.beans.HospitalBean;
import ocr.medhead.emergency.beans.PatientBean;
import ocr.medhead.emergency.domain.Emergency;
import ocr.medhead.emergency.domain.EmergencyDTO;
import ocr.medhead.emergency.service.EmergencyService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@ExtendWith(MockitoExtension.class)
class EmergencyControllerTests {

    @Autowired
    private WebTestClient webTestClient;

    private MockMvc mvc;

    @Mock
    private EmergencyService emergencyService;

    @InjectMocks
    private EmergencyController emergencyController;

    JacksonTester<List<Emergency>> jsonEmergencies;
    JacksonTester<Object> jsonToObject;

    private List<Emergency> emergenciesList;
    private List<HospitalBean> hospitalsList;
    private PatientBean expectedPatient;
    private Emergency expectedEmergency;
    private EmergencyDTO userInputsWithHospitalNameAndAvailableBed;
    private EmergencyDTO userInputsWithHospitalNameAndNoBedAvailable;
    private EmergencyDTO userInputsWithHospitalNameAndSpecialityAndAvailableBed;
    private EmergencyDTO userInputsWithHospitalNameAndSpecialityAndNoBedAvailable;
    private EmergencyDTO userInputsWithSpecialityAndAvailableBed;
    private EmergencyDTO userInputsWithPatienIdOnly;

    @SneakyThrows
    @BeforeEach
    public void setup() {

        // Utility method to initialize JacksonTester fields
        JacksonTester.initFields(this, new ObjectMapper());

        // MockMvc standalone approach for unit tests
        mvc = MockMvcBuilders.standaloneSetup(emergencyController)
                //.setControllerAdvice(new EmergencyExceptionHandler())
                .addFilters(new EmergencyFilter())
                .build();

        emergenciesList = new ArrayList<>();
        emergenciesList.add(new Emergency(1, 1, "Brooks", "Fred",1, "Fred Brooks", "cardiologie", true));
        emergenciesList.add(new Emergency(2, 2, "Crusher", "Julia",2, "Julia Crusher", "cardiologie", true));
        emergenciesList.add(new Emergency(3, 3, "Bashir", "Beverly",3, "Beverly Bashir", "immunologie", true));

        hospitalsList = new ArrayList<>();
        hospitalsList.add(new HospitalBean(1,"Fred Brooks", 2, new String[] {"cardiologie", "immunologie"}));
        hospitalsList.add(new HospitalBean(2,"Julia Crusher", 0, new String[] {"cardiologie"}));
        hospitalsList.add(new HospitalBean(3,"Beverly Bashir", 5, new String[] {"immunologie", "neuropathologie diagnostique"}));

        expectedPatient = new PatientBean(1, "Brooks", "Fred");//(HashMap<?, ?>) jsonToObject.parseObject("{\"id\":1,\"surname\":\"Brooks\",\"name\":\"Fred\"}");

        expectedEmergency = new Emergency(4, (int) expectedPatient.getId(), expectedPatient.getSurname(), expectedPatient.getName(), hospitalsList.get(0).getId(), hospitalsList.get(0).getName(), null, true);

        webTestClient = WebTestClient.bindToController(new EmergencyController(emergencyService)).build();

        userInputsWithHospitalNameAndAvailableBed = new EmergencyDTO(1, "Fred Brooks", null);
        userInputsWithHospitalNameAndNoBedAvailable = new EmergencyDTO(1, "Julia Crusher", null);
        userInputsWithHospitalNameAndSpecialityAndAvailableBed = new EmergencyDTO(1, "Fred Brooks", "cardiologie");
        userInputsWithHospitalNameAndSpecialityAndNoBedAvailable = new EmergencyDTO(1, "Julia Crusher", "cardiologie");
        userInputsWithSpecialityAndAvailableBed = new EmergencyDTO(1, null, "immunologie");
        userInputsWithPatienIdOnly = new EmergencyDTO(1, "", "");

    }

    @Test
    void canRetrieveAllEmergencies() throws Exception {
        // Given
        given(emergencyService.findAll()).willReturn(emergenciesList);

        // When
        MockHttpServletResponse response = mvc
                .perform(get("/emergencies")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Then
        then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        then(response.getContentAsString()).isEqualTo(jsonEmergencies.write(emergenciesList).getJson());

    }

    @Test
    void canRecordAnEmergencyByHospitalNameAndAvailableBed() {
        // Given
        Mockito.when(emergencyService.checkingAvailabilityOfBed(userInputsWithHospitalNameAndAvailableBed)).thenReturn(Mono.just(expectedEmergency));

        // When
        webTestClient.post().uri("/emergencies")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(userInputsWithHospitalNameAndAvailableBed))
                .exchange()
                .expectStatus().isCreated()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.id").isNotEmpty()
                .jsonPath("$.id").isEqualTo(4)
                .jsonPath("$.patientSurname").isEqualTo("Brooks")
                .jsonPath("$.hospitalId").isNotEmpty()
                .jsonPath("$.hospitalId").isEqualTo(1)
                .jsonPath("$.hospitalName").isEqualTo("Fred Brooks");

        // Then
        Mockito.verify(emergencyService, times(1)).checkingAvailabilityOfBed(userInputsWithHospitalNameAndAvailableBed);

    }

    @Test
    void canRecordAnEmergencyByHospitalNameAndNoBedAvailable() {
        // Given
        Mockito.when(emergencyService.checkingAvailabilityOfBed(userInputsWithHospitalNameAndNoBedAvailable)).thenReturn(Mono.error(new Exception()));

        // When
        webTestClient.post().uri("/emergencies")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(userInputsWithHospitalNameAndNoBedAvailable))
                .exchange()
                .expectStatus().is5xxServerError();

        // Then
        Mockito.verify(emergencyService, times(1)).checkingAvailabilityOfBed(userInputsWithHospitalNameAndNoBedAvailable);

    }

    @Test
    void canRecordAnEmergencyByHospitalNameAndSpecialityAndAvailableBed() {
        // Given
        Mockito.when(emergencyService.checkingAvailabilityOfBed(userInputsWithHospitalNameAndSpecialityAndAvailableBed)).thenReturn(Mono.just(expectedEmergency));

        // When
        webTestClient.post().uri("/emergencies")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(userInputsWithHospitalNameAndSpecialityAndAvailableBed))
                .exchange()
                .expectStatus().isCreated();

        // Then
        Mockito.verify(emergencyService, times(1)).checkingAvailabilityOfBed(userInputsWithHospitalNameAndSpecialityAndAvailableBed);

    }

    @Test
    void canRecordAnEmergencyByHospitalNameAndSpecialityAndNoBedAvailable() {
        // Given
        Mockito.when(emergencyService.checkingAvailabilityOfBed(userInputsWithHospitalNameAndSpecialityAndNoBedAvailable)).thenReturn(Mono.error(new Exception()));

        // When
        webTestClient.post().uri("/emergencies")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(userInputsWithHospitalNameAndSpecialityAndNoBedAvailable))
                .exchange()
                .expectStatus().is5xxServerError();

        // Then
        Mockito.verify(emergencyService, times(1)).checkingAvailabilityOfBed(userInputsWithHospitalNameAndSpecialityAndNoBedAvailable);

    }

    @Test
    void canRecordAnEmergencyBySpecialityAndAvailableBed() {
        // Given
        Mockito.when(emergencyService.checkingAvailabilityOfBed(userInputsWithSpecialityAndAvailableBed)).thenReturn(Mono.just(expectedEmergency));

        // When
        webTestClient.post().uri("/emergencies")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(userInputsWithSpecialityAndAvailableBed))
                .exchange()
                .expectStatus().isCreated();

        // Then
        Mockito.verify(emergencyService, times(1)).checkingAvailabilityOfBed(userInputsWithSpecialityAndAvailableBed);

    }

    @Test
    void cannotCreateEmergencyWithPatientIdOnly() throws Exception {
        // Given
        Mockito.when(emergencyService.checkingAvailabilityOfBed(userInputsWithPatienIdOnly)).thenReturn(Mono.error(new Exception()));

        // When
        webTestClient.post().uri("/emergencies")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(userInputsWithPatienIdOnly))
                .exchange()
                .expectStatus().is5xxServerError();

        // Then
        Mockito.verify(emergencyService, times(1)).checkingAvailabilityOfBed(userInputsWithPatienIdOnly);

    }
}