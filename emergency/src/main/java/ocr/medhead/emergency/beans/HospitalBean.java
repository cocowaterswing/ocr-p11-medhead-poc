package ocr.medhead.emergency.beans;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HospitalBean {

    private int id;

    private String name;

    private int availableBeds;

    private String[] specialities;

    public HospitalBean(){
        // Empty constructor
    }

    public HospitalBean(int id, String name, int availableBeds, String[] specialities) {
        this.id = id;
        this.name = name;
        this.availableBeds = availableBeds;
        this.specialities = specialities;
    }
}
