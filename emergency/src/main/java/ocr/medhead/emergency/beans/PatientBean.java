package ocr.medhead.emergency.beans;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class PatientBean {

    private int id;

    private String surname;

    private String name;

    public PatientBean() {
        // Empty constructor
    }
}
