package ocr.medhead.emergency.domain;

import lombok.*;

/**
 * Attempt coming from the user
 */

@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class EmergencyDTO {
    int patientId;
    String patientLocation;
    String hospitalSpeciality;

    public EmergencyDTO(){
        // Add this default constructor to fix the error during test execution : "Cannot construct instance of `ocr.medhead.emergency.domain.EmergencyDTO` (no Creators, like default constructor, exist)"
    }
}
