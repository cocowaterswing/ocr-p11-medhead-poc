package ocr.medhead.emergency.domain;

import lombok.*;

/**
 * Identifies the attempt from a user to book a bed.
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class Emergency {
    private int id;
    private int patientId;
    private String patientSurname;
    private String patientName;
    private int hospitalId;
    private String hospitalName;
    private String hospitalSpeciality;
    private boolean isAvailableBed;
}
