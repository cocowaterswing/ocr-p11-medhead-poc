package ocr.medhead.emergency.proxies;

import ocr.medhead.emergency.beans.HospitalBean;
import ocr.medhead.emergency.exceptions.HospitalFallback;
import ocr.medhead.emergency.exceptions.EmergencyNotFoundException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "hospital", fallback = HospitalFallback.class)
public interface HospitalMSProxy {

    @GetMapping("/hospitals/{name}/beds")
    HospitalBean getHospitalByNameAndAvailableBeds(@PathVariable("name") String name) throws EmergencyNotFoundException;

    @GetMapping("/hospitals/{hospitalName}/specialities/{specialityName}")
    HospitalBean getHospitalByNameAndSpeciality(@PathVariable("hospitalName") String hospitalName, @PathVariable("specialityName") String specialityName) throws EmergencyNotFoundException;

    @GetMapping("/hospitals/specialities/{name}/beds")
    HospitalBean getHospitalsBySpecialityAndAvailableBed(@PathVariable("name") String name) throws EmergencyNotFoundException;

    @GetMapping("/hospitals/{id}/beds/emergency")
    HospitalBean substractOneBedForEmergency(@PathVariable int id) throws EmergencyNotFoundException;
}