package ocr.medhead.emergency.proxies;

import ocr.medhead.emergency.beans.PatientBean;
import ocr.medhead.emergency.exceptions.EmergencyNotFoundException;
import ocr.medhead.emergency.exceptions.PatientFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "patient", fallback = PatientFallback.class)
public interface PatientMSProxy {

    @GetMapping(value = "/patients/{id}")
    PatientBean getPatientById(@PathVariable("id") int id) throws EmergencyNotFoundException;

}
