package ocr.medhead.emergency.controller;

import lombok.extern.slf4j.Slf4j;
import ocr.medhead.emergency.domain.Emergency;
import ocr.medhead.emergency.domain.EmergencyDTO;
import ocr.medhead.emergency.service.EmergencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;

@Slf4j
@RestController
public class EmergencyController {

    final EmergencyService emergencyService;

    @Autowired
    public EmergencyController(EmergencyService emergencyService) {
        this.emergencyService = emergencyService;
    }

    @GetMapping("/emergencies")
    public List<Emergency> getEmergenciesList() {
        return emergencyService.findAll();
    }

    @PostMapping("/emergencies")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Emergency> search(@RequestBody EmergencyDTO userDemand) {

        return emergencyService.checkingAvailabilityOfBed(userDemand);

    }

}
