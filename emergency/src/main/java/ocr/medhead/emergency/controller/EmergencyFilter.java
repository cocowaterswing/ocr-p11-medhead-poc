package ocr.medhead.emergency.controller;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EmergencyFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        var httpServletResponse = (HttpServletResponse) response;
        httpServletResponse.setHeader("EMERGENCY-APP", "emergency-header");
        chain.doFilter(request, response);
    }
}
