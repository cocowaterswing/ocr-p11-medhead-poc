package ocr.medhead.emergency.service;

import ocr.medhead.emergency.domain.Emergency;
import ocr.medhead.emergency.domain.EmergencyDTO;
import ocr.medhead.emergency.exceptions.EmergencyNotFoundException;
import reactor.core.publisher.Mono;

import java.util.List;

public interface EmergencyService {

    Mono<Emergency> checkingAvailabilityOfBed(EmergencyDTO userDemand);

    List<Emergency> findAll();

}
