package ocr.medhead.emergency.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ocr.medhead.emergency.beans.HospitalBean;
import ocr.medhead.emergency.beans.PatientBean;
import ocr.medhead.emergency.domain.Emergency;
import ocr.medhead.emergency.domain.EmergencyDTO;
import ocr.medhead.emergency.repository.emergency.EmergencyRepository;
import ocr.medhead.emergency.repository.hospital.HospitalRepository;
import ocr.medhead.emergency.repository.patient.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.List;

import static java.lang.String.valueOf;

@Service
@Slf4j
@RequiredArgsConstructor
public class EmergencyServiceImpl implements EmergencyService{

    @Autowired
    private HospitalRepository hospitalRepository;

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private EmergencyRepository emergencyRepository;


    @Override
    public Mono<Emergency> checkingAvailabilityOfBed(EmergencyDTO userDemand) {

        final Emergency newEmergency = new Emergency();
        final PatientBean defaultPatient = new PatientBean(999, "UNKNOWN", "PATIENT");

        Mono<PatientBean> patient = patientRepository.findPatient(userDemand.getPatientId()).onErrorReturn(defaultPatient);
        Mono<HospitalBean> hospital = hospitalRepository.findHospital(userDemand.getPatientLocation(), userDemand.getHospitalSpeciality());

        return Mono.zip(patient, hospital).flatMap(response -> {
            newEmergency.setPatientSurname(response.getT1().getSurname());
            newEmergency.setPatientName(response.getT1().getName());
            newEmergency.setPatientId(response.getT1().getId());
            newEmergency.setHospitalId(response.getT2().getId());
            newEmergency.setHospitalName(response.getT2().getName());
            newEmergency.setHospitalSpeciality(userDemand.getHospitalSpeciality());
            newEmergency.setAvailableBed(true);
            emergencyRepository.save(newEmergency);
            return Mono.just(newEmergency);
        });

    }

    @Override
    public List<Emergency> findAll() {
        return emergencyRepository.findAll();
    }

}
