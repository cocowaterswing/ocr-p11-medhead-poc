package ocr.medhead.emergency.repository.hospital;

import ocr.medhead.emergency.beans.HospitalBean;
import ocr.medhead.emergency.exceptions.EmergencyNotFoundException;
import ocr.medhead.emergency.proxies.HospitalMSProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class HospitalRepository {

    @Autowired
    private DiscoveryClient discoveryClient;

    // OPENFEIGN IMPLEMENTATION
    //@Autowired
    //private HospitalMSProxy hospitalMSProxy;
    /*public HospitalBean findHospital(String hospitalName, String hospitalSpeciality) throws EmergencyNotFoundException {

        Boolean nameAndNoSpeciality = !hospitalName.isEmpty() && hospitalSpeciality.isEmpty();
        Boolean nameAndSpecialty = !hospitalName.isEmpty() && !hospitalSpeciality.isEmpty();
        Boolean noNameAndSpeciality = hospitalName.isEmpty() && !hospitalSpeciality.isEmpty();

        HospitalBean hospital = new HospitalBean();

        if (Boolean.TRUE.equals(nameAndNoSpeciality)) {
            hospital = hospitalMSProxy.getHospitalByNameAndAvailableBeds(hospitalName);
        } else if (Boolean.TRUE.equals(nameAndSpecialty)) {
            hospital = hospitalMSProxy.getHospitalByNameAndSpeciality(hospitalName, hospitalSpeciality);
        } else if (Boolean.TRUE.equals(noNameAndSpeciality)) {
            hospital = hospitalMSProxy.getHospitalsBySpecialityAndAvailableBed(hospitalSpeciality);
        }

        if (hospital.getId() != 0)
            hospitalMSProxy.substractOneBedForEmergency(hospital.getId());
            return hospital;

    }*/

    public Mono<HospitalBean> findHospital(String patientLocation, String hospitalSpeciality) {

        Boolean nameAndNoSpeciality = !patientLocation.isEmpty() && hospitalSpeciality.isEmpty();
        Boolean nameAndSpecialty = !patientLocation.isEmpty() && !hospitalSpeciality.isEmpty();
        Boolean noNameAndSpeciality = patientLocation.isEmpty() && !hospitalSpeciality.isEmpty();

        String hospitalURL = discoveryClient.getInstances("hospital").get(0).getUri().toString();
        WebClient webClient = WebClient.create(hospitalURL);

        String hospitalUri = "";

        if (Boolean.TRUE.equals(nameAndNoSpeciality)) {
            hospitalUri = "/hospitals/" + patientLocation + "/beds";
        } else if (Boolean.TRUE.equals(nameAndSpecialty)) {
            hospitalUri = "/hospitals/" + patientLocation + "/specialities/" + hospitalSpeciality;
        } else if (Boolean.TRUE.equals(noNameAndSpeciality)) {
            hospitalUri = "/hospitals/specialities/" + hospitalSpeciality + "/beds";
        }

        return webClient.get()
                .uri(hospitalUri)
                .retrieve()
                .onStatus(status -> status.value() == HttpStatus.NOT_FOUND.value(),
                        response -> Mono.error(new EmergencyNotFoundException("Unable to find a bed."))
                )
                .bodyToMono(HospitalBean.class);

    }

}
