package ocr.medhead.emergency.repository.patient;

import ocr.medhead.emergency.beans.PatientBean;
import ocr.medhead.emergency.exceptions.EmergencyNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class PatientRepository {

    @Autowired
    private DiscoveryClient discoveryClient;

    public Mono<PatientBean> findPatient(int patientId) throws EmergencyNotFoundException {
        String patientURL = discoveryClient.getInstances("patient").get(0).getUri().toString();
        WebClient webClient = WebClient.create(patientURL);
        return webClient.get().uri("/patientsEmergency/" + patientId).retrieve()
                .onStatus(status -> status.value() == HttpStatus.NOT_FOUND.value(),
                response -> Mono.error(new EmergencyNotFoundException("Unable to find a patient with this id.")))
                .bodyToMono(PatientBean.class);
    }

}
