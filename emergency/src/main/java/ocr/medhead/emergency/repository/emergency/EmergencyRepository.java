package ocr.medhead.emergency.repository.emergency;

import lombok.extern.slf4j.Slf4j;

import ocr.medhead.emergency.domain.Emergency;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class EmergencyRepository {

    final List<Emergency> emergencies;

    public EmergencyRepository(){
        emergencies = new ArrayList<>();
        emergencies.add(new Emergency(1, 1, "Brooks", "Fred",1, "Fred Brooks", "cardiologie", true));
        emergencies.add(new Emergency(2, 2, "Crusher", "Julia",2, "Julia Crusher", "cardiologie", true));
        emergencies.add(new Emergency(3, 3, "Bashir", "Beverly",3, "Beverly Bashir", "immunologie", true));

        log.info("Generating a list of emergencies : {}", emergencies);
    }

    public List<Emergency> findAll() {
        return emergencies;
    }

    public Emergency save(Emergency emergency) {
        emergency.setId(emergencies.size()+1);
        emergencies.add(emergency);
        return emergencies.get(emergencies.size()-1);
    }

}
