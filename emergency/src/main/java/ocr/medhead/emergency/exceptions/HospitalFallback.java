package ocr.medhead.emergency.exceptions;

import ocr.medhead.emergency.beans.HospitalBean;
import ocr.medhead.emergency.proxies.HospitalMSProxy;
import org.springframework.stereotype.Component;

@Component
public class HospitalFallback implements HospitalMSProxy {
    @Override
    public HospitalBean getHospitalByNameAndAvailableBeds(String name) throws EmergencyNotFoundException {
        throw new EmergencyNotFoundException("No bed available for this hospital.");
    }

    @Override
    public HospitalBean getHospitalByNameAndSpeciality(String hospitalName, String specialityName) throws EmergencyNotFoundException {
        throw new EmergencyNotFoundException("No bed available for this hospital with this speciality.");
    }

    @Override
    public HospitalBean getHospitalsBySpecialityAndAvailableBed(String name) throws EmergencyNotFoundException {
        throw new EmergencyNotFoundException("No bed available for this speciality.");
    }

    @Override
    public HospitalBean substractOneBedForEmergency(int id) throws EmergencyNotFoundException {
        throw new EmergencyNotFoundException("Unable to reserve a bed.");
    }
}
