package ocr.medhead.emergency.exceptions;

import ocr.medhead.emergency.beans.PatientBean;
import ocr.medhead.emergency.proxies.PatientMSProxy;
import org.springframework.stereotype.Component;

@Component
public class PatientFallback implements PatientMSProxy {
    @Override
    public PatientBean getPatientById(int id) throws EmergencyNotFoundException {
        throw new EmergencyNotFoundException("No patient with this id.");
    }
}
