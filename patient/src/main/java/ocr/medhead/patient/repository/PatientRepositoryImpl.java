package ocr.medhead.patient.repository;

import lombok.extern.slf4j.Slf4j;
import ocr.medhead.patient.domain.Patient;
import ocr.medhead.patient.exceptions.NonExistingPatientException;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class PatientRepositoryImpl implements PatientRepository {

    private List<Patient> patients;

    public PatientRepositoryImpl() {
        patients = new ArrayList<>();
        patients.add(new Patient(1, "Brooks", "Fred"));
        patients.add(new Patient(2, "Crusher", "Julia"));
        patients.add(new Patient(3, "Bashir", "Beverly"));

        log.info("Generating a list of patients: {}", patients);
    }

    @Override
    public List<Patient> findAll() {
        return patients;
    }

    @Override
    public Patient save(Patient newPatient) {
        patients.add(newPatient);
        return patients.get(patients.size()-1);
    }

    @Override
    public Patient findByPatientSurname(String surname) {
        for (Patient patient : patients)
            if (surname.equals(patient.getSurname()))
                return patient;
        throw new NonExistingPatientException();
    }

    @Override
    public Patient updatePatient(Patient updatedPatient) {
        patients.set(updatedPatient.getId()-1, updatedPatient);
        return patients.get(updatedPatient.getId()-1);
    }

    @Override
    public Patient deleteById(int id) {
        if (id > patients.size() || id < 1) throw new NonExistingPatientException();
        return patients.remove(id-1);
    }

    @Override
    public Patient findById(int id) {
        if (id > patients.size() || id < 1) throw new NonExistingPatientException();
        return patients.get(id-1);
    }

    @Override
    public Mono<Patient> findByIdEmergency(int id) {
        if (id <= patients.size() & id >= 1) {
            return Mono.just(patients.get(id-1));
        }
        throw new NonExistingPatientException();
    }
}
