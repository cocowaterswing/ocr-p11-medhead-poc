package ocr.medhead.patient.repository;

import ocr.medhead.patient.domain.Patient;
import reactor.core.publisher.Mono;

import java.util.List;

public interface PatientRepository {
    List<Patient> findAll();

    Patient save(Patient newPatient);

    Patient findByPatientSurname(String surname);

    Patient updatePatient(Patient updatedPatient);

    Patient deleteById(int id);

    Patient findById(int id);
    Mono<Patient> findByIdEmergency(int id);
}
