package ocr.medhead.patient.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * Stores information to identify the patient.
 */
@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class Patient {
    private int id;
    private String surname;
    private String name;

    public Patient() {
        // Add this default constructor to fix the error during test execution : "Cannot construct instance of `ocr.medhead.emergency.domain.EmergencyDTO` (no Creators, like default constructor, exist)"
    }
}