package ocr.medhead.patient.controller;

import ocr.medhead.patient.exceptions.NonExistingPatientException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class PatientExceptionHandler {

    @ExceptionHandler(NonExistingPatientException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public final ResponseEntity<String> handleNonExistingPatient(){
        String error = "Unable to find this patient.";
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

}