package ocr.medhead.patient.controller;

import ocr.medhead.patient.domain.Patient;
import ocr.medhead.patient.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
public class PatientController {

    private final PatientRepository patientRepository;

    @Autowired
    public PatientController(PatientRepository hospitalRepository) {
        this.patientRepository = hospitalRepository;
    }

    @GetMapping("/patients")
    public List<Patient> getPatientsList() {
        return patientRepository.findAll();
    }

    @GetMapping("/patients/{id}")
    public Patient getPatientById(@PathVariable int id) {
        return patientRepository.findById(id);
    }

    @GetMapping("/patientsEmergency/{id}")
    public Mono<Patient> getPatientByIdForEmergency(@PathVariable int id) {
        return patientRepository.findByIdEmergency(id);
    }

    @PostMapping("/patients")
    @ResponseStatus(HttpStatus.CREATED)
    public Patient newPatient(@RequestBody Patient newPatient) {
        return patientRepository.save(newPatient);
    }

    @PutMapping("/patients/{id}")
    public Patient updatedPatient(@RequestBody Patient updatedPatient) {
        return patientRepository.updatePatient(updatedPatient);
    }

    @DeleteMapping("/patients/{id}")
    public void deletePatient(@PathVariable int id) {
        patientRepository.deleteById(id);
    }

    @GetMapping("/patients/surname/{surname}")
    public Patient getPatientBySurname(@PathVariable String surname){
        return patientRepository.findByPatientSurname(surname);
    }
}
