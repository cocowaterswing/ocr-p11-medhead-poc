package ocr.medhead.patient.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import ocr.medhead.patient.domain.Patient;
import ocr.medhead.patient.exceptions.NonExistingPatientException;
import ocr.medhead.patient.repository.PatientRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;

import java.rmi.ServerError;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@ExtendWith(MockitoExtension.class)
class PatientControllerTests {

    private MockMvc mvc;

    @Mock
    private PatientRepository patientRepository;

    @Autowired
    private WebTestClient webTestClient;

    @InjectMocks
    private PatientController patientController;

    JacksonTester<Patient> jsonPatient;
    JacksonTester<List<Patient>> jsonPatients;

    private List<Patient> expectedPatients;
    Patient expectedPatient;


    @BeforeEach
    void setup() {
        expectedPatients = new ArrayList<>();
        expectedPatients.add(new Patient(1,"Brooks", "Fred"));
        expectedPatients.add(new Patient(2,"Crusher", "Julia"));
        expectedPatients.add(new Patient(3,"Bashir", "Beverly"));

        expectedPatient = new Patient(1,"Brooks", "Fred");

        JacksonTester.initFields(this, new ObjectMapper());

        // MockMvc standalone approach for unit tests
        mvc = MockMvcBuilders.standaloneSetup(patientController)
                .setControllerAdvice(new PatientExceptionHandler())
                .addFilters(new PatientFilter())
                .build();
    }

    @Test
    void canRetrieveAllPatients() throws Exception {
        // Given
        given(patientRepository.findAll()).willReturn(expectedPatients);

        // When
        MockHttpServletResponse response = mvc
                .perform(get("/patients")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Then
        then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        then(response.getContentAsString()).isEqualTo(jsonPatients.write(expectedPatients).getJson());
    }

    @Test
    void canCreateANewPatient() throws Exception {
        // Given
        given(patientRepository.save(expectedPatient)).willReturn(expectedPatient);

        // When
        MockHttpServletResponse response = mvc
                .perform(post("/patients")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonPatient.write(expectedPatient).getJson()))
                .andReturn()
                .getResponse();

        // Then
        then(response.getStatus()).isEqualTo(HttpStatus.CREATED.value());
        then(response.getContentAsString()).isEqualTo(jsonPatient.write(expectedPatient).getJson());
    }

    @Test
    void canRetrieveBySurnameWhenExists() throws Exception {
        // Given
        given(patientRepository.findByPatientSurname("Brooks")).willReturn(expectedPatient);

        // When
        MockHttpServletResponse response = mvc
                .perform(get("/patients/surname/Brooks")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();
        // Then
        then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        then(response.getContentAsString()).isEqualTo(jsonPatient.write(expectedPatient).getJson());
    }

    @Test
    void canRetrieveBySurnameWhenDoesNotExist() throws Exception {
        // Given
        given(patientRepository.findByPatientSurname("DoesNotExist")).willThrow(new NonExistingPatientException());

        // When
        MockHttpServletResponse response = mvc
                .perform(get("/patients/surname/DoesNotExist")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        // Then
        then(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
        then(response.getContentAsString()).isEqualTo("Unable to find this patient.");
    }

    @Test
    void canUpdateAPatient() throws Exception {
        // Given
        given(patientRepository.updatePatient(expectedPatient)).willReturn(expectedPatient);

        // When
        MockHttpServletResponse response = mvc
                .perform(put("/patients/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonPatient.write(expectedPatient).getJson()))
                .andReturn()
                .getResponse();

        // Then
        then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        then(response.getContentAsString()).isEqualTo(jsonPatient.write(expectedPatient).getJson());
    }

    @Test
    void canDeleteAPatientById() throws Exception {
        // Given
        given(patientRepository.deleteById(1)).willReturn(new Patient());

        // When
        MockHttpServletResponse response = mvc
                .perform(delete("/patients/1")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        // Then
        then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void canDeleteAnNonExistingPatientById() throws Exception {
        // Given
        given(patientRepository.deleteById(1)).willThrow(new NonExistingPatientException());

        // When
        MockHttpServletResponse response = mvc
                .perform(delete("/patients/1")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        // Then
        then(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
        then(response.getContentAsString()).isEqualTo("Unable to find this patient.");
    }

    @Test
    void canGetPatientById() throws Exception {
        // Given
        given(patientRepository.findById(1)).willReturn(expectedPatient);

        // When
        MockHttpServletResponse response = mvc
                .perform(get("/patients/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        // Then
        then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        then(response.getContentAsString()).isEqualTo(jsonPatient.write(expectedPatient).getJson());
    }

    @Test
    void canGetPatientByIdForEmergency() {

        Mockito.when(patientRepository.findByIdEmergency(1)).thenReturn(Mono.just(expectedPatient));

        webTestClient = WebTestClient.bindToController(new PatientController(patientRepository)).build();

        webTestClient.get().uri("/patientsEmergency/{id}", 1)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.name").isNotEmpty()
                .jsonPath("$.id").isEqualTo(1)
                .jsonPath("$.name").isEqualTo("Fred")
                .jsonPath("$.surname").isEqualTo("Brooks");

        Mockito.verify(patientRepository, times(1)).findByIdEmergency(1);

    }

    @Test()
    void canGetErrorWhenIdForEmergencyNotExisting() {

        Mockito.when(patientRepository.findByIdEmergency(10)).thenReturn(Mono.error(new Exception()));

        webTestClient = WebTestClient.bindToController(new PatientController(patientRepository)).build();

        webTestClient.get().uri("/patientsEmergency/{id}", 10)
                .exchange()
                .expectStatus().is5xxServerError();

        Mockito.verify(patientRepository, times(1)).findByIdEmergency(10);

    }

}
