package ocr.medhead.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.CrossOrigin;

@SpringBootApplication
@EnableDiscoveryClient
public class GatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatewayApplication.class, args);
	}

	// TODO Create a circuit breaker and tests https://spring.io/guides/gs/gateway/
	@Bean
	public RouteLocator myRoutes(RouteLocatorBuilder builder) {

		return builder.routes()
				.route(p -> p
						.path("/emergencies/*").or().path("/emergencies")
						.uri("lb://emergency"))
				.route(p -> p
						.path("/hospitals/*").or().path("/hospitals")
						.uri("lb://hospital"))
				.route(p -> p
						.path("/patients/*").or().path("/patients")
						.uri("lb://patient"))
				.build();
	}

}