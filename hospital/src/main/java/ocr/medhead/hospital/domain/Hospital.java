package ocr.medhead.hospital.domain;

import lombok.*;

/**
 * This class represents an Hospital to take care of an emergency.
 */
@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class Hospital {
    private int id;
    private String name;
    private int availableBeds;
    private String[] specialities;

    public Hospital(){
        // Add this default constructor to fix the error during test execution : "Cannot construct instance of `ocr.medhead.emergency.domain.EmergencyDTO` (no Creators, like default constructor, exist)"
    }

    public void removeOneBed() {
        if (availableBeds > 0) {
            availableBeds -= 1;
        }
    }
}
