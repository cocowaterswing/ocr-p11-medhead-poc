package ocr.medhead.hospital.controller;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class HospitalFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        var httpServletResponse = (HttpServletResponse) response;
        httpServletResponse.setHeader("HOSPITAL-APP", "hospital-header");
        chain.doFilter(request, response);
    }

}
