package ocr.medhead.hospital.controller;

import ocr.medhead.hospital.exceptions.HospitalNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class HospitalExceptionHandler {

    @ExceptionHandler(HospitalNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<String> handleNonExistingHospital(){
        String error = "Unable to find this hospital.";
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

}
