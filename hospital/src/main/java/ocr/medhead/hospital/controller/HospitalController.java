package ocr.medhead.hospital.controller;

import ocr.medhead.hospital.domain.Hospital;
import ocr.medhead.hospital.repository.HospitalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
final class HospitalController {

    private final HospitalRepository hospitalRepository;

    @Autowired
    public HospitalController(HospitalRepository hospitalRepository) {
        this.hospitalRepository = hospitalRepository;
    }

    @GetMapping("/hospitals")
    public List<Hospital> getHospitalsList() {
        return hospitalRepository.findAll();
    }

    @PostMapping("/hospitals")
    @ResponseStatus(HttpStatus.CREATED)
    public Hospital newHospital(@RequestBody Hospital newHospital) {
        return hospitalRepository.save(newHospital);
    }

    @PutMapping("/hospitals/{id}")
    public Hospital updatedHospital(@RequestBody Hospital updatedHospital) {
        return hospitalRepository.update(updatedHospital);
    }

    @DeleteMapping("/hospitals/{id}")
    public void deleteHospital(@PathVariable int id) {
        hospitalRepository.deleteById(id);
    }

    @GetMapping("/hospitals/{name}")
    public Hospital getHospitalByName(@PathVariable String name){
        return hospitalRepository.findBylocation(name);
    }

    @GetMapping("/hospitals/{name}/beds")
    public Mono<Hospital> getHospitalByNameAndAvailableBeds(@PathVariable String name){
        return hospitalRepository.findByLocationAndBeds(name);
    }

    @GetMapping("/hospitals/beds")
    public List<Hospital> getHospitalWithAvailableBeds(){
        return hospitalRepository.findByAvailableBeds();
    }

    @GetMapping("/hospitals/specialities/{name}")
    public List<Hospital> getHospitalsBySpeciality(@PathVariable String name){
        return hospitalRepository.findBySpeciality(name);
    }

    @GetMapping("/hospitals/specialities/{name}/beds")
    public Mono<Hospital> getHospitalsBySpecialityAndAvailableBed(@PathVariable String name){
        return hospitalRepository.findBySpecialityAndBeds(name);
    }

    @GetMapping("/hospitals/{hospitalName}/specialities/{specialityName}")
    public Mono<Hospital> getHospitalByNameAndSpeciality(@PathVariable String hospitalName, @PathVariable String specialityName){
        return hospitalRepository.findByNameAndSpecialityAndBeds(hospitalName, specialityName);
    }

    @GetMapping("/hospitals/{id}/beds/emergency")
    public Mono<Hospital> substractOneBedForEmergency(@PathVariable int id) {
        return hospitalRepository.substractOneBedForEmergency(id);
    }
}