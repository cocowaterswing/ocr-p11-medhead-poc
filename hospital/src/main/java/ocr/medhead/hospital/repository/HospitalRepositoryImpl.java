package ocr.medhead.hospital.repository;

import lombok.extern.slf4j.Slf4j;
import ocr.medhead.hospital.domain.Hospital;
import ocr.medhead.hospital.exceptions.HospitalNotFoundException;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Component
public class HospitalRepositoryImpl implements HospitalRepository {

    private List<Hospital> hospitals;

    public HospitalRepositoryImpl() {
        hospitals = new ArrayList<Hospital>();
        hospitals.add(new Hospital(1,"Fred Brooks", 20, new String[] {"cardiologie", "immunologie"}));
        hospitals.add(new Hospital(2,"Julia Crusher", 0, new String[] {"cardiologie"}));
        hospitals.add(new Hospital(3,"Beverly Bashir", 50, new String[] {"immunologie", "neuropathologie diagnostique"}));

        log.info("Generating a list of hospitals: {}", hospitals);
    }

    @Override
    public List<Hospital> findAll() {
        return hospitals;
    }

    @Override
    public Hospital save(Hospital newHospital) {
        hospitals.add(newHospital);
        return hospitals.get(hospitals.size()-1);
    }

    @Override
    public Hospital findBylocation(String location) {
        for (Hospital hospital : hospitals)
            if (location.equals(hospital.getName()))
                return hospital;
        throw new HospitalNotFoundException();
    }

    @Override
    public Mono<Hospital> findByLocationAndBeds(String name) {
        for (Hospital hospital : hospitals)
            if (hospital.getName().equals(name) && hospital.getAvailableBeds() > 0){
                substractOneBedForEmergency(hospital.getId());
                return Mono.just(hospital).log();
            }

        throw new HospitalNotFoundException();
    }

    @Override
    public List<Hospital> findBySpeciality(String name) {
        List<Hospital> filteredHospitals = new ArrayList<>();
        for (Hospital hospital : hospitals)
            if (Arrays.asList(hospital.getSpecialities()).contains(name))
                filteredHospitals.add(hospital);
        if (filteredHospitals.isEmpty())
            throw new HospitalNotFoundException();
        else
            return filteredHospitals;
    }

    @Override
    public Mono<Hospital> findBySpecialityAndBeds(String name) {
        Hospital filteredHospital = new Hospital();
        for (Hospital hospital : hospitals)
            if (Arrays.asList(hospital.getSpecialities()).contains(name) && hospital.getAvailableBeds() > 0 && filteredHospital.getAvailableBeds() < hospital.getAvailableBeds())
                filteredHospital = hospital;
        if (filteredHospital.equals(new Hospital()))
            throw new HospitalNotFoundException();
        else {
            substractOneBedForEmergency(filteredHospital.getId());
            return Mono.just(filteredHospital).log();
        }

    }

    @Override
    public Mono<Hospital> findByNameAndSpecialityAndBeds(String hospitalName, String specialityName) {
        for (Hospital hospital : hospitals)
            if (hospital.getName().equals(hospitalName) && Arrays.asList(hospital.getSpecialities()).contains(specialityName) && hospital.getAvailableBeds() > 0) {
                substractOneBedForEmergency(hospital.getId());
                return Mono.just(hospital).log();
            }

        throw new HospitalNotFoundException();
    }

    @Override
    public Hospital update(Hospital updatedHospital) {
        hospitals.set(updatedHospital.getId()-1, updatedHospital);
        return hospitals.get(updatedHospital.getId()-1);
    }

    @Override
    public Hospital deleteById(int id) {
        if (id > hospitals.size() || id < 1) throw new HospitalNotFoundException();
        return hospitals.remove(id-1);
    }

    @Override
    public List<Hospital> findByAvailableBeds() {
        List<Hospital> hospitalsWithBeds = this.hospitals.stream().filter(h -> h.getAvailableBeds() > 0).toList();
        if (hospitals.isEmpty()) throw new HospitalNotFoundException();
        return hospitalsWithBeds;
    }

    @Override
    public Mono<Hospital> substractOneBedForEmergency(int id) {
        if (id > hospitals.size() || id < 1) throw new HospitalNotFoundException();
        hospitals.get(id-1).removeOneBed();
        return Mono.just(hospitals.get(id-1));
    }
}
