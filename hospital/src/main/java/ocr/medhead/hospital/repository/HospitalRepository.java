package ocr.medhead.hospital.repository;

import ocr.medhead.hospital.domain.Hospital;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface HospitalRepository {

    List<Hospital> findAll();

    Hospital save(Hospital newHospital);

    Hospital findBylocation(String name);

    Mono<Hospital> findByLocationAndBeds(String name);

    List<Hospital> findBySpeciality(String name);

    Mono<Hospital> findBySpecialityAndBeds(String name);

    Mono<Hospital> findByNameAndSpecialityAndBeds(String hospitalName, String specialityName);

    Hospital update(Hospital updatedHospital);

    Hospital deleteById(int id);

    List<Hospital> findByAvailableBeds();

    Mono<Hospital> substractOneBedForEmergency(int id);
}
