package ocr.medhead.hospital.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import ocr.medhead.hospital.domain.Hospital;
import ocr.medhead.hospital.exceptions.HospitalNotFoundException;
import ocr.medhead.hospital.repository.HospitalRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@ExtendWith(MockitoExtension.class)//springextension??
class HospitalControllerTests {

    private MockMvc mvc;

    @Mock
    private HospitalRepository hospitalRepository;

    @Autowired
    private WebTestClient webTestClient;

    @InjectMocks
    private HospitalController hospitalController;

    JacksonTester<Hospital> jsonHospital;
    JacksonTester<List<Hospital>> jsonHospitals;

    private List<Hospital> expectedHospitals;
    Hospital expectedHospital;
    String errorMessage = "Unable to find this hospital.";

    @BeforeEach
    void setup() {
        expectedHospitals = new ArrayList<>();
        expectedHospitals.add(new Hospital(1,"Fred Brooks", 2, new String[] {"cardiologie", "immunologie"}));
        expectedHospitals.add(new Hospital(2,"Julia Crusher", 0, new String[] {"cardiologie"}));
        expectedHospitals.add(new Hospital(3,"Beverly Bashir", 5, new String[] {"immunologie", "neuropathologie diagnostique"}));

        expectedHospital = new Hospital(1,"Fred Brooks", 2, new String[] {"cardiologie", "immunologie"});

        webTestClient = WebTestClient.bindToController(new HospitalController(hospitalRepository)).build();

        JacksonTester.initFields(this, new ObjectMapper());

        // MockMvc standalone approach for unit tests
        mvc = MockMvcBuilders.standaloneSetup(hospitalController)
                .setControllerAdvice(new HospitalExceptionHandler())
                .addFilters(new HospitalFilter())
                .build();
    }

    @Test
    void canRetrieveAllHospitals() throws Exception {
        // Given
        given(hospitalRepository.findAll()).willReturn(expectedHospitals);

        // When
        MockHttpServletResponse response = mvc
                .perform(get("/hospitals")
                    .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Then
        then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        then(response.getContentAsString()).isEqualTo(jsonHospitals.write(expectedHospitals).getJson());
    }

    @Test
    void canCreateANewHospital() throws Exception {
        // Given
        given(hospitalRepository.save(expectedHospital)).willReturn(expectedHospital);

        // When
        MockHttpServletResponse response = mvc
                .perform(post("/hospitals")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonHospital.write(expectedHospital).getJson()))
                .andReturn()
                .getResponse();

        // Then
        then(response.getStatus()).isEqualTo(HttpStatus.CREATED.value());
        then(response.getContentAsString()).isEqualTo(jsonHospital.write(expectedHospital).getJson());
    }

    @Test
    void canRetrieveByNameWhenExists() throws Exception {
        // Given
        given(hospitalRepository.findBylocation("Fred Brooks")).willReturn(expectedHospital);

        // When
        MockHttpServletResponse response = mvc
                .perform(get("/hospitals/Fred Brooks")
                    .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();
        // Then
        then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        then(response.getContentAsString()).isEqualTo(jsonHospital.write(expectedHospital).getJson());
    }

    @Test
    void canRetrieveByNameWhenDoesNotExist() throws Exception {
        // Given
        given(hospitalRepository.findBylocation("DoesNotExist")).willThrow(new HospitalNotFoundException());

        // When
        MockHttpServletResponse response = mvc
                .perform(get("/hospitals/DoesNotExist")
                    .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        // Then
        then(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
        then(response.getContentAsString()).isEqualTo(errorMessage);
    }

    @Test
    void canUpdateAnHospital() throws Exception {
        // Given
        given(hospitalRepository.update(expectedHospital)).willReturn(expectedHospital);

        // When
        MockHttpServletResponse response = mvc
                .perform(put("/hospitals/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonHospital.write(expectedHospital).getJson()))
                .andReturn()
                .getResponse();

        // Then
        then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        then(response.getContentAsString()).isEqualTo(jsonHospital.write(expectedHospital).getJson());
    }

    @Test
    void canDeleteAnHospitalById() throws Exception {
        // Given
        given(hospitalRepository.deleteById(1)).willReturn(new Hospital());

        // When
        MockHttpServletResponse response = mvc
                .perform(delete("/hospitals/1")
                    .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        // Then
        then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        then(response.getContentAsString()).isEmpty();
    }

    @Test
    void canDeleteAnNonExistingHospitalById() throws Exception {
        // Given
        given(hospitalRepository.deleteById(1)).willThrow(new HospitalNotFoundException());

        // When
        MockHttpServletResponse response = mvc
                .perform(delete("/hospitals/1")
                    .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        // Then
        then(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
        then(response.getContentAsString()).isEqualTo(errorMessage);
    }

    @Test
    void canGetHospitalByNameAndAvailableBeds() throws Exception {

        Mockito.when(hospitalRepository.findByLocationAndBeds("Fred Brooks")).thenReturn(Mono.just(expectedHospital));

        webTestClient.get().uri("/hospitals/{name}/beds", "Fred Brooks")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.name").isNotEmpty()
                .jsonPath("$.id").isEqualTo(1)
                .jsonPath("$.name").isEqualTo("Fred Brooks")
                .jsonPath("$.availableBeds").isEqualTo(2);

        Mockito.verify(hospitalRepository, times(1)).findByLocationAndBeds("Fred Brooks");

    }

    @Test
    void canGetHospitalByNameAndAvailableBedsWhenNoBedsAvailable() throws Exception {
        // Given
        given(hospitalRepository.findByLocationAndBeds("Fred Brooks")).willThrow(new HospitalNotFoundException());

        // When
        MockHttpServletResponse response = mvc
                .perform(get("/hospitals/Fred Brooks/beds")
                    .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        // Then
        then(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
        then(response.getContentAsString()).isEqualTo(errorMessage);
    }

    @Test
    void canGetHospitalsWithAvailableBeds() throws Exception {
        // Given
        given(hospitalRepository.findByAvailableBeds()).willReturn(expectedHospitals);

        // When
        MockHttpServletResponse response = mvc
                .perform(get("/hospitals/beds")
                    .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        // Then
        then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        then(response.getContentAsString()).contains("Fred Brooks").contains("2").contains("cardiologie");
        then(response.getContentAsString()).isEqualTo(jsonHospitals.write(expectedHospitals).getJson());
    }

    @Test
    void canGetHospitalsWithAvailableBedsWhenThereAreNoBed() throws Exception {
        // Given
        given(hospitalRepository.findByAvailableBeds()).willThrow(new HospitalNotFoundException());

        // When
        MockHttpServletResponse response = mvc
                .perform(get("/hospitals/beds")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        // Then
        then(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
        then(response.getContentAsString()).isEqualTo(errorMessage);
    }

    @Test
    void canGetHospitalsBySpeciality() throws Exception {
        // Given
        given(hospitalRepository.findBySpeciality("cardiologie")).willReturn(expectedHospitals);

        // When
        MockHttpServletResponse response = mvc
                .perform(get("/hospitals/specialities/cardiologie")
                    .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        // Then
        then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        then(response.getContentAsString()).contains("Fred Brooks").contains("2").contains("cardiologie");
        then(response.getContentAsString()).isEqualTo(jsonHospitals.write(expectedHospitals).getJson());
    }

    @Test
    void canGetHospitalsBySpecialityWhenDoesNotExist() throws Exception {
        // Given
        given(hospitalRepository.findBySpeciality("orthodontie")).willThrow(new HospitalNotFoundException());

        // When
        MockHttpServletResponse response = mvc
                .perform(get("/hospitals/specialities/orthodontie")
                    .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        // Then
        then(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
        then(response.getContentAsString()).isEqualTo(errorMessage);
    }

    @Test
    void canGetHospitalByNameAndSpeciality() {

        String[] choices = new String[2];
        choices[0] = "Fred Brooks";
        choices[1] = "cardiologie";

        Mockito.when(hospitalRepository.findByNameAndSpecialityAndBeds("Fred Brooks","cardiologie")).thenReturn(Mono.just(expectedHospital));

        webTestClient.get().uri("/hospitals/{name}/specialities/{speciality}", choices)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.name").isNotEmpty()
                .jsonPath("$.id").isEqualTo(1)
                .jsonPath("$.name").isEqualTo("Fred Brooks")
                .jsonPath("$.availableBeds").isEqualTo(2);

        Mockito.verify(hospitalRepository, times(1)).findByNameAndSpecialityAndBeds("Fred Brooks","cardiologie");

    }

    @Test
    void canGetHospitalByNameAndSpecialityWhenDoesNotExist() throws Exception {

        String[] choices = new String[2];
        choices[0] = "Fred Brooks";
        choices[1] = "orthodontie";

        Mockito.when(hospitalRepository.findByNameAndSpecialityAndBeds("Fred Brooks","orthodontie")).thenReturn(Mono.error(new Exception()));

        webTestClient.get().uri("/hospitals/{name}/specialities/{speciality}", choices)
                .exchange()
                .expectStatus().is5xxServerError();

        Mockito.verify(hospitalRepository, times(1)).findByNameAndSpecialityAndBeds("Fred Brooks","orthodontie");

    }

    @Test
    void substractOneBedForEmergency() throws Exception {

        // When
        MockHttpServletResponse response = mvc
                .perform(get("/hospitals/1/beds/emergency")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        // Then
        then(response.getStatus()).isEqualTo(HttpStatus.OK.value());

    }

    @Test
    void canGetHospitalBySpecialityAndBiggerAvailableBeds() {

        Mockito.when(hospitalRepository.findBySpecialityAndBeds("immunologie")).thenReturn(Mono.just(expectedHospitals.get(2)));

        webTestClient.get().uri("/hospitals/specialities/{speciality}/beds", "immunologie")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.name").isNotEmpty()
                .jsonPath("$.id").isEqualTo(3)
                .jsonPath("$.name").isEqualTo("Beverly Bashir")
                .jsonPath("$.availableBeds").isEqualTo(5);
                //.jsonPath("$.specialities").isEqualTo(new String[] {"immunologie", "neuropathologie diagnostique"});

        Mockito.verify(hospitalRepository, times(1)).findBySpecialityAndBeds("immunologie");

    }
}
